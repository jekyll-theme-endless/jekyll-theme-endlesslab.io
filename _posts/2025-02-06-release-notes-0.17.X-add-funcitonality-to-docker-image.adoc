= (Version `0.17.X`) Add Functionality to Docker Image
:page-layout: post
:page-tags: [release notes, features, Dockerfile, npm, Yarn, zip, unzip, dependency manager, mermaid.js, AsciiDoc, diagram, nvm, Node.js, _configData.yml, Sankey]
:page-last_modified_at: 2025-02-14
:page-permalink: /release-0.17.X/
:page-summary: "Release `0.17.X`: Added support for `zip`, `unzip`, and the package managers `npm` and `Yarn`. Additionally, mermaid diagrams are now supported in AsciiDoc."

== New feature

* Added support for the `zip` and `unzip` commands in the Docker image.
* Added dependency managers `npm` and `Yarn` to the Docker image.
* Added support for mermaid diagrams in AsciiDoc.
* Added the `nvm` (Node Version Manager) to the Docker image, to install the latest Node.js version.


== Updates, fixes and improvements

* Fixed broken link to the documentation.
* Renamed the file `_data.yml` to `_configData.yml`.
* Version `0.17.1`: Updated the Mermaid example in the link:/asciidoc-showroom/#mermaid[AsciiDoc Showroom].
* Version `0.17.2`: Add example of a Sankey diagram to the link:/asciidoc-showroom/#mermaid[AsciiDoc Showroom].

