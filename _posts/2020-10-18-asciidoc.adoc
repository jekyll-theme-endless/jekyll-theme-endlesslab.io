= Example: Asciidoc<b>!</b>
:page-tags: [AsciiDoc, example, test-page, front matter, posts, pages, escaping, permalink]
// Do not use HTML special characters in tags when setting them with AsciiDoc variables.
// Reason: AsciiDoc directly escapes them, so a tag like "A&B" becomes "A&amp;B" internally.
// If you want to use HTML special characters in your tag, set them in the Jekyll front matter instead.
// See the release notes of version 0.12.0 for more details.
//:page-tags: [AsciiDoc, example, test-page, front matter, posts, pages, escaping, A&B]
:page-last_modified_at: 2024-10-30
:page-permalink: /asciidoc-example/
:page-summary: "Example AsciiDoc post: Shows layout defaults, permalink settings, handling special chars in tags, and configuration options for titles, summaries, and dates."

Layout defaults to `post` for files in the `_posts`-folder.
The title (`page.title`) of AsciiDoc files defaults to the H1-heading of pages and posts.
H1-heading (e.g. `= Heading`) is removed from the content.

The date of the last edit can be added by setting a value for the page variable `last_modified_at`:

[source, asciidoc]
----
:page-last_modified_at: 2024-10-30
----





== Example code for ...

Below, you'll find several example documents for posts and pages.
Several lines are annotated with comments to help you understand the available configuration options for each type of page.
You can also use this code as a template for your own content.





=== ... an AsciiDoc *page*

Below, you'll find the annotated code for a basic AsciiDoc page.



[source, asciidoc]
----
= Title of an <em>AsciiDoc</em> **__page__** with `code`<!--1-->
:page-layout: page<!--2-->
:page-permalink: /asciidoc-page-template-1/<!--3-->
:experimental:<!--4-->
:toc: preamble<!--5-->
:toclevels: 5<!--6-->
:page-published: true<!--7-->
:page-summary: "150-160 characters"<!--8-->
// page specific settings:
:page-menu_label: AsciiDoc<!--9-->
:page-menu_position: -1<!--10-->

Preamble text.

== Heading 1

Paragraph 1.

Paragraph 2.

== Heading 2

Paragraph 3.
----
<1> Page heading (i.e. the title of your page).
AsciiDoc uses this to determine the value for the `page.title` variable.
Special characters are escaped by AsciiDoc. (here e.g. `<em>`)
AsciiDoc commands are supported. (here: bold, italic and code)

<2> For documents outside the `_posts` folder, the default layout is `page`.
Thus, you typically don't specify this setting.

<3> Permalinks create stable, user-friendly URLs that improve SEO and make it easier
for users and search engines to locate and share content.
In AsciiDoc it's good practice to have slashes at the beginning and end.

<4> If experimental features like keyboard shortcuts and menu paths are used,
you have to activate them using `:experimental:`.

<5> `:toc:` adds a **t**able **o**f **c**ontents (TOC) to your page.

<6> With `:toclevels:`, you can configure up to which heading level entries will be displayed in the table of contents.
A value of 1 corresponds to `<h2>`, a value of 2 to `<h3>`, and so on.

<7> You can prevent your posts from being published on the blog by setting `:page-published: false`.
In the `_drafts` folder, the default value is false.
Everywhere else, the default value is `true`, so you don't need to set this option.

<8> The page summary is used for the meta description and can also be used for brief descriptions in blog post lists.
Keep the following points in mind:
* 150-160 characters, no HTML special characters
* Call to Action: Use active language, e.g., "Read…", "Learn about…"
* Content: Concise, natural summary, avoid buzzwords

<9> Label to be used for the link in the automatically generated menu.
If it not specified, the page title is used.

<10> Value to determine the position in the menu.
The larger the number, the further left the entry appears.
A negative number hides the page from the menu.





=== ... an AsciiDoc *post*

Below, you'll find the annotated code for a basic AsciiDoc post.
These documents are typically stored in the `_posts` folder.


[source, asciidoc]
----
= Title of an <em>AsciiDoc</em> **__post__** with `code`
:page-layout: post<!--1-->
:page-permalink: /asciidoc-post-template-1/
:experimental:
//:toc: preamble<!--2-->
//:toclevels: 3
:page-published: true
:page-summary: "150-160 characters"
// post specific settings:
:page-tags: [AsciiDoc, example, tags]<!--3-->
:page-date: 2022-07-13<!--4-->
:page-last_modified_at: 2024-10-30<!--5-->

Preamble text.

== Heading 1

Paragraph 1.

Paragraph 2.

== Heading 2

Paragraph 3.
----
<1> For documents within the `_posts` folder, the default layout is `post`.
Thus, you typically don't specify this setting.

<2> You can use a table of contents (TOC) in your articles.
However, blog posts generally shouldn't be so long that this is necessary.
Therefore, I have disabled this option using the comment symbol `//`.

<3> List of keywords by which this article should be found in the tag cloud.
+
CAUTION: If you want to use keywords with HTML special characters (e.g, the `&` in `A&B`),
you should *not* set the tags via the AsciiDoc variable but instead through the Jekyll front matter
(see the link:#tag-example[next code example] for details).

<4> Creation date of the article.
This typically comes from the filename but can be overridden by specifying it with `date`.
The recommended format is *ISO 8601*, such as `YYYY-MM-DD`, `YYYY-MM-DD HH:MM:SS`, or `YYYY-MM-DD HH:MM:SS +/-ZZZZ`.

<5> Timestamp of the last edit.
This time is used, for example, in the RSS feed.
The format is the same as that of `date`.





[[tag-example]]
=== ... an AsciiDoc *post* (with special chars in tags)

In the following source code for a blog article, a tag (`A&B`) is used with an HTML special character (`&`).
For this, the `tags` variable is populated in the Jekyll front matter, rather than using the AsciiDoc page attribute `:page-tags:` as in the previous examples.

TIP: Jekyll front matter is written in *YAML*.
Therefore, note that while the syntax is similar, there are slight differences.
The `page-` prefix is omitted, and the leading colon used for page attributes in AsciiDoc is also absent in Jekyll front matter.

Also, check the source code of the post link:/asciidoc-example-with-special-chars-in-tags/[Example: Asciidoc post with special chars in tags],
where the `A&B` example is applied for this blog.


.Code of an AsciiDoc post with a tag contaning an HTML special char.
[source, asciidoc]
----
---//<1>
tags: [AsciiDoc, example, tags, A&B]<!--2-->
---
= Title of an AsciiDoc post with special chars in tags
:page-permalink: /asciidoc-post-template-2/
:page-last_modified_at: 2024-10-29
:page-summary: "150-160 characters"

AsciiDoc content of your post.
----
<1> Jekyll front matter can be combined with AsciiDoc variables.
+
IMPORTANT: Variables in the front matter are **not** escaped by Jekyll.
Values inferred and set through the processing of AsciiDoc page attributes are escaped.
If you want to move the title `:page-title: 'What is a GmbH & Co KG'` to the Jekyll front matter, you would need to write it as `title: 'What is a GmbH &amp;amp; Co KG'`.

<2> Unlike the `title`, `tags` is the only Jekyll front matter variable that is escaped upon output.
Therefore, you can enter the unescaped values here.



