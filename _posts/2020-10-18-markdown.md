---
layout: post
title: "Example: Markdown<b>!</b> and &lt;i&gt;"
# In the Jekyll front matter, tags can be set directly with HTML special characters.
# They are escaped on output using the `escape` Liquid filter in the layouts.
tags: [Markdown, example, test-page, front matter, posts, pages, escaping, A&B]
#
## Some more edge cases
#tags: [Markdown, example, test-page, front matter, posts, pages, escaping, "&", "<i>", "a/B", "a:b"]
#
## Known bug: Currently, escaped HTML special chars like `&amp;` and `&lt;` cannot be used in tags.
## Although the pages are generated correctly, the links to them do not work.
#tags: [Markdown, example, test-page, front matter, posts, pages, escaping, "&", "<i>", "special char &amp;", "&lt;"]
last_modified_at: 2024-10-30
permalink: /markdown-example/
summary: "Example Markdown post: Demonstrates front matter usage, setting titles, handling HTML special characters, and tracking last edit dates in Markdown pages."
---
In Markdown the layout does not default to `post`. Thus, front matter must be set.
The title (`page.title`) of Markdown **posts** defaults to the name of the post-file.
The (`page.title`)  of Markdown **pages** does **not** default to the name of page files.
H1-Heading (e.g. `# Heading`) is part of the content.

The date of the last edit can be set by adding the following code to the front matter:

```yml
last_modified_at: 2024-10-30
```


## Example code for ...

Here are examples of pages created with AsciiDoc.
For explanations of the commands, check out the [AsciiDoc example]({{ "/asciidoc-example/" | relative_url }}).
Just be aware that the syntax is slightly different, as the Jekyll front matter is written in YAML.
In general, I recommend creating pages and posts with AsciiDoc.

### ... a Markdown ***page***

```markdown
---
layout: page
title: "Example: Markdown<b>!</b> and &lt;i&gt;"
permalink: /markdown-example-page-1/
published: true
# page specific settings:
:page-menu_label: MarkdownExample
:page-menu_position: -1
---

Preamble text.

== Heading 1

Paragraph 1.

Paragraph 2.

== Heading 2

Paragraph 3.
```



### ... a Markdown ***post***

```markdown
---
layout: post
title: "Example: Markdown<b>!</b> and &lt;i&gt;"
permalink: /markdown-example-page-1/
published: true
# post specific settings:
tags: [Markdown, example, test-page, front matter, posts, escaping, A&B]
last_modified_at: 2024-10-30
---

Preamble text.

== Heading 1

Paragraph 1.

Paragraph 2.

== Heading 2

Paragraph 3.
```


