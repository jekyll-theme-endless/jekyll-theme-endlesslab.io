= (Version `0.8.X`) New Functionality
:page-layout: post
:page-tags: [features, release notes, function, documentation, EditorConfig]
:page-permalink: /release-0.8.X/
:page-summary: "Release 0.8.X: Jekyll theme 'endless' now supports content additions without layout edits, includes EditorConfig, keyboard shortcut examples, and screenshots."

New functionality.

== New features

* Allow adding content to the layout without having to reimplement/copy layout files of the theme.
* Added EditorConfig file (see: https://editorconfig.org/)
* Added examples for keyboard shortcuts and menu paths
* Added screenshots
