module Jekyll
	module LiquidMatchFilter
		def match(input, regex)
			result = input.match(Regexp.new(regex)) ? true : false
			Jekyll.logger.debug("LiquidMatchFilter", "[DEBUG] Match-filter called with input '#{input}' and regex '#{regex}' returns #{result}")
			# return the result
			result
  	end

	end

end

