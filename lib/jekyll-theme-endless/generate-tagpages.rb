# Generator

# Generates a page for each tag listed in `site.tags`.
# The files created are by default named `tags/<tagname>/index.html`.
# The content is generated using the layout file `_layouts/page-tag.html`

# The following values can be set in `_config.yml`
# `tag_dir`
#   * the name of the directory in which the files for each tag are created;
#   * default: `tags`
# `tag_title_prefix`
#   * Prefix to be used for the title of the page
#   * default: `Tag: `

# Necessary files:
# `_layouts/page-tag.html` - used to generate content of tag files.

# The following values are made available in the layout:
#   * `page.tag`   - contains the tag
#   * `page.title` - contains the generated title, e.g. "Tag: <tagname>"

# NOTE: after changes to the plugin, `jekyll serve` must be started again.

# See also: https://jekyllrb.com/docs/plugins/
# See also: https://jekyllrb.com/docs/plugins/generators/

# Required to escape strings
require 'cgi'

module Jekyll

	module Endless

		# TagPageGenerator is a subclass of Generator
		class TagPageGenerator < Generator
			safe true
			# A Generator needs to implement the generate method
			def generate(site)
				Jekyll.logger.info("[INFO] Starting TagPage generation")
				# If a layout with the name `page-tag` exists
				if site.layouts.key? 'page-tag'
					Jekyll.logger.info("TagPageGenerator", "[INFO] 'page-tag' layout found")
					# The directory in which the files are to be created is configured in `site.tag_dir`.
					# If not, the directory `tags/` is used.
					dir = site.config['tag_dir'] || 'tags'
					Jekyll.logger.info("TagPageGenerator", "[INFO] Using tag directory: #{dir}")

					# For each tag in the tag-list:
					site.tags.each_key do |tag|
						# Create a page-object using TagPage and add it to the `site.pages` array
						site.pages << TagPage.new(site, site.source, File.join(dir, tag), tag)
						Jekyll.logger.debug("TagPageGenerator", "[DEBUG] TagPage created for tag: #{tag}")
					end

					Jekyll.logger.info("TagPageGenerator", "[INFO] All tags processed successfully")
				else
					Jekyll.logger.error("TagPageGenerator", "[ERROR] Could not find the layout 'page-tag'. Create a file '_layouts/page-tag.html' to fix this error. Skipping TagPage generation for now.")
				end
				Jekyll.logger.info("TagPageGenerator", "[INFO] TagPage generation finished")
			end
		end

		# TagPage is a subclass of Page
		# It is used in the `TagPageGenerator`
		class TagPage < Page
			def initialize(site, base, dir, tag)
				# Define instance variables ('@') to make values available in the super-class `Page`.
				# The variables are available in the layout as e.g. `page.name`.
				@site = site
				@base = base
				@dir	= dir
				@name = 'index.html'

				self.process(@name)
				Jekyll.logger.debug("TagPageGenerator", "[DEBUG] Generating file in folder: #{dir}")
				self.read_yaml(File.join(base, '_layouts'), 'page-tag.html')

				# The prefix for the generated title is set via `tag_title_prefix` in `_config.yml` and defaults to `Tag: `
				tag_title_prefix = site.config['tag_title_prefix'] || 'Tag: '
				# Generates the title for the tag page and makes it available in the layout file as `page.title`
				# Since AsciiDoc escapes the title, the plugin must also escape the content of the title (otherwise the tag would be unescaped).
				self.data['title'] = CGI.escapeHTML("#{tag_title_prefix}#{tag}")
				Jekyll.logger.debug("TagPageGenerator", "[DEBUG] Generating title for tag page: #{self.data['title']}")
				# Makes `page.tag` available in the layout file
				self.data['tag'] = tag

				# Make `site.tags[tag]` available as `page.postlist` in the layout
				# The use of `uniq` ensures that an entry does not appear twice in the list of "posts tagged with this tag"
				# if a tag has been accidentally entered twice in the tag list of a post.
				self.data['postlist'] = (site.tags[tag] || []).uniq

				#Jekyll.logger.debug("TagPageGenerator", "[DEBUG] Loaded self.data: #{self.data.inspect}")
			end
		end

	end

end


