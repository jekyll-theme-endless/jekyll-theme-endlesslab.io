# This is the initialization file of the gem

require_relative 'jekyll-theme-endless/generate-tagpages'
require_relative 'jekyll-theme-endless/liquid-match-filter'

module Jekyll

	# Namespace for Endless Theme
	module Endless
		# Additional initialization code for Endless can go here
		Jekyll.logger.info("TagPageGenerator", "[INFO] TagPageGenerator has been loaded")
	end

	# Namespace for custom Liquid filters
	module LiquidMatchFilter
		# Additional initialization code for Liquid filters can go here
		Liquid::Template.register_filter(Jekyll::LiquidMatchFilter)

		Jekyll.logger.info("LiquidMatchFilter", "[INFO] LiquidMatchFilter has been loaded")
	end

end


