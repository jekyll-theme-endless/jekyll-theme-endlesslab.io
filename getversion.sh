#!/usr/bin/env ruby

# Load the file containing the version number
require_relative 'lib/jekyll-theme-endless/version'

# Output of the version number
puts Jekyll::Endless::VERSION

