---
layout: statistics
title: About this Website
menu_label: About
menu_position: 100
summary: "Discover my journey in building a Jekyll theme for effortless blogging. Focus on content creation—customize freely and join in to make it even better!"
---
My motivation behind this project was to create an easy-to-use blogging pipeline with AsciiDoc.
Jekyll was the ideal tool to achieve this, so I developed this theme to meet all my requirements and to gradually transition my websites to it.
The goal is to focus on content creation rather than getting caught up in technical implementation.

If you'd like to use this theme, feel free to explore the documentation and examples provided.
You're welcome to customize it to fit your needs, and contributions are always appreciated to improve and expand the theme for everyone.

