#!/usr/bin/env bash

# Script to perform git and gem commands.
# Takes the version number that is defined in "lib/jekyll-theme-endless/version"

# Get the version number
VERSION=$(./getversion.sh)

echo "Test if the version ${VERSION} matches SemVer";
! [[ ${VERSION} =~ ^[0-9]*\.[0-9]*\.[0-9]*$ ]] && echo "The version number does not match #.#.#" && exit 1

echo "Stage all changes"
git add .

echo "Commit changes to the local git repository";
git commit -m "Publish version ${VERSION} on https://rubygems.org/"  || exit 1

echo "Add version-tag '${VERSION}' to the last commit";
git tag ${VERSION} || exit 1

echo "Push commits to remote repository";
git push || exit 1

echo "Push tags to remote repository";
git push --tags || exit 1

echo "Build the gem";
gem build "jekyll-theme-endless.gemspec" || exit 1

echo "Push the Gem to rubygems.org repository";
gem push "jekyll-theme-endless-${VERSION}.gem" || exit 1

