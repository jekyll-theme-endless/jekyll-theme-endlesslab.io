# This Dockerfile creates a Docker image that includes all the necessary tools
# for the 'jekyll-theme-endless' project.

# Use the official Ruby base image
FROM ruby:latest

# * Update system packages
# * Install required packages for asciidoctor-diagram:
#   ** https://packages.debian.org/bookworm/graphviz
#   ** https://packages.debian.org/bookworm/plantuml (Depends on default-jre, meaning Java must be available!)
#   ** https://packages.debian.org/bookworm/lilypond
#   ** https://packages.debian.org/bookworm/yarnpkg
#   ** https://packages.debian.org/bookworm/openssh-client
#   ** https://packages.debian.org/bookworm/chromium (is required for Mermaid.js)
#   ** https://packages.debian.org/bookworm/rsync
# * Clean up package lists to reduce image size.
# * Install additional dependencies required for working with Ruby:
#   ** The bundler gem.
#
# Combining these into a single RUN command minimizes the number of layers in the image.

# Set environment variables for NVM (Node Version Manager)
ENV NVM_DIR="/root/.nvm"

RUN apt-get update && \
		apt-get install -y --no-install-recommends \
			chromium \
			curl \
			graphviz \
			lilypond \
			openssh-client \
			plantuml \
			rsync \
			unzip \
			yarnpkg \
			zip && \
		rm -rf /var/lib/apt/lists/*

# Download and install the latest version of Node Version Manager (NVM)
ADD https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.1/install.sh /tmp/install.sh
RUN bash /tmp/install.sh && \
		# Add source commands to the .profile file to make NVM available in login shells
		echo 'echo ".profile was loaded"' >> /root/.profile && \
		echo 'export NVM_DIR="$HOME/.nvm"' >> /root/.profile && \
		echo '[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"' >> /root/.profile && \
		echo '[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"' >> /root/.profile && \
		# Load NVM during installation
		. "$NVM_DIR/nvm.sh" && \
		# Install the latest Long-Term Support (LTS) version of Node.js and the latest version of NPM
		nvm install --lts && \
		nvm use --lts && \
		# Install mermaid-cli globally using npm
		npm install -g @mermaid-js/mermaid-cli && \
		# Provide a Puppeteer configuration to allow Mermaid to run inside a Docker container
		echo '{ "args": ["--no-sandbox","--disable-setuid-sandbox"] }' > /root/puppeteer_config.json && \
		# Install Bundler (`gem` is provided by the Ruby base image)
		gem install bundler

# Start the shell as a login shell so that .profile scripts are executed when the Docker container starts.
# This ensures NVM functions are available, e.g., for GitLab CI runner scripts.
ENTRYPOINT [ "/bin/bash", "-c", "exec bash --login" ]
