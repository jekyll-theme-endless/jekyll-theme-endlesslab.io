---
layout: page
menu_label: Markdown
menu_position: 150
permalink: /markdown-showroom/
summary: "Explore the Markdown showroom: See Markdown's capabilities in this theme, including code, tables, lists, links, and quotes for clean, simple formatting."
---
# Markdown showroom

Here are a few examples in Markdown.
While Markdown has significantly fewer features compared to other markup languages like AsciiDoc,
it still offers a simple and efficient way to write and format blog posts.
It's especially useful for quickly creating clean, readable content without much overhead.
Jekyll uses **Kramdown** as its Markdown processor.
Therefore, you can also use Kramdown syntax.

For more information, you can check out

* [The official Markdown syntax guide](https://daringfireball.net/projects/markdown/syntax)
* [Wikipedia article about Markdown](https://en.wikipedia.org/wiki/Markdown)
* [Kramdown Syntax](https://kramdown.gettalong.org/syntax.html)
* [Kramdown Quick Reference](https://kramdown.gettalong.org/quickref.html)
* [Kramdown Cheat Sheet](https://aoterodelaroza.github.io/devnotes/kramdown-cheatsheet/)



## Source code listings

Python source code:

```python
# Python code
def greet(name):
    print(f"Hello, {name}!")

greet('World')
```

Ruby code listings using Kramdown syntax:

~~~
puts "Hello! What's your name?"
# gets user input and removes the newline
name = gets.chomp
puts "Hi, #{name}!"
~~~
{: .language-ruby}


~~~ ruby
# Define the function
def square(number)
  number * number
end

# Use the function
puts "Enter a number to square:"
num = gets.chomp.to_i
result = square(num)

puts "The square of #{num} is #{result}"
~~~




## Text formatting

* Text with **strong** significance to me
* *Emphasized* text
* ***Combination*** of both
* You can also format parts of a word:
  * **C**reate, **R**ead, **U**pdate, and **D**elete (CRUD)
  * Counter*clock*wise
  * `string`s
* ~~This is strikethrough text~~
* `monospace text`





## Links

* <https://example.com>
* [Link to example.com](https://example.com)
* <mailto:test@example.com>
* [send a test message to example.com](mailto:test@example.com)





## Tables

A simple table:

| Name             | Age   | City        |
|------------------|-------|-------------|
| Max Mustermann   | 30    | Berlin      |
| Maria Musterfrau | 28    | München     |
| John Doe         | 45    | New York    |



A table with alignment and some markdown features:

| Left          | Center        | Right         |
|:--------------|:-------------:|--------------:|
| Text A        | Text B        | Text C        |
| AAAAAAAAAAAAA | BBBBBBBBBBBBB | CCCCCCCCCCCCC |
| **Formatting** | _is_ | ***supported*** |
| Links | are also supported | [https://example.com](https://example.com) |

<!--
Cells in Markdown do not need to have the same width.
However, rows must not be separated by blank lines.
At the end of the table, there must be a blank line.
-->





## Lists

### Unordered

- Item 1
- Item 2
  - Item 2.1
  - Item 2.2
  - Item 2.3
- Item 3

### Numbered

1. Numbered item 1
2. Numbered item 2
   7. Numbered item 2.1
   6. Numbered item 2.2
   9. Numbered item 2.3
666. Numbered item 3

### Checklist

* [x] checked: `* [x]` (lower case x)
* [X] checked: `* [X]` (or upper case X)
- [x] checked: `- [x]`
- [ ] unchecked: `- [ ]`
* [ ] unchecked: `* [ ]`
  * item without checkbox
  * [ ] unchecked item on level 2: `* [ ]`
    * [x] checked item on level 3: `* [x]`




## Images

![Alterntive Text for the image](/assets/images/tree-3822149_640.jpg "Title of the image.")





## Quotes

> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
> Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
>
> Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.





## Glossary

Term 1
: Definition for Term 1.

Term 2
: Definition for Term 2.
Text can have multiple lines.
They can also contain lists ...
* item 1
* item 2
* item 3
> ... and they can also contain quotes.


: Another definition for Term 2.
