# Load version information from lib directory
require File.absolute_path 'lib/jekyll-theme-endless/version', __dir__

# frozen_string_literal: true

Gem::Specification.new do |spec|

	spec.name          = "jekyll-theme-endless"
	spec.version       = Jekyll::Endless::VERSION
	spec.authors       = ["Sven Boekhoff"]
	spec.email         = ["rubygems.org@boekhoff.net"]

	spec.summary       = "A Jekyll theme with support for AsciiDoc, mathematical formulas (e.g., AsciiMath, LaTeX), text-based diagrams (e.g., Ditaa, Graphviz), and more. Explore its features at https://jekyll-theme-endless.gitlab.io/."
	spec.homepage      = "https://gitlab.com/jekyll-theme-endless/jekyll-theme-endless.gitlab.io"
	spec.license       = "MIT"

	spec.files         = `git ls-files -z`.split("\x0").select do |f|
		# Select files in the specified directories
		f.match(%r!^(assets|lib|_data|_layouts|_plugins|_includes|_sass|pages_examples|LICENSE|README|_config\.yml)!i) &&
		# Exclude specific files
		# NOTE: In Ruby, the - character does not need to be escaped in regular expressions
		# unless it's used within a character class (e.g., [a-z]), where it acts as a range operator.
		# !f.match(%r!^_data/(tag-description\.yml|anotherfile\.yml)$!i)
		!f.match(%r!^_data/(tag-description\.yml)$!i)
	end
	spec.require_paths = ['lib']

	spec.required_ruby_version = ">= 3.1.6"

	# Add the AsciiDoc Plugin to this ruby project
  spec.add_runtime_dependency "jekyll-asciidoc", "~> 3.0", '>= 3.0.1'
	# Require Jekyll >= 4.1
	spec.add_runtime_dependency "jekyll", "~> 4.3", '>= 4.3.4'
	# Add the Rouge syntax-highlighter to this ruby project
  spec.add_runtime_dependency "rouge", "~> 4.5", '>= 4.5.1'
	# Add RSS-feed functionality to the Jekyll-blog
  spec.add_runtime_dependency "jekyll-feed", "~> 0.17", '>= 0.17.0'
	# AsciiDoctor Diagram
	spec.add_runtime_dependency 'asciidoctor-diagram', '~> 2.3', '>= 2.3.1'

	# Fix warnings caused by (transitive) dependencies.
	# logger was loaded from the standard library, but will no longer be part of the default gems starting from Ruby 3.5.0.
	# csv was loaded from the standard library, but will no longer be part of the default gems starting from Ruby 3.4.0.
	# ostruct was loaded from the standard library, but will no longer be part of the default gems starting from Ruby 3.5.0.
	# base64 was loaded from the standard library, but will no longer be part of the default gems starting from Ruby 3.4.0.
	spec.add_runtime_dependency 'logger', '~> 1.6', '>= 1.6.0'
	spec.add_runtime_dependency 'csv', '~> 3.2', '>= 3.2.8'
	spec.add_runtime_dependency 'ostruct', '~> 0.6', '>= 0.6.0'
	spec.add_runtime_dependency 'base64', '~> 0.2', '>= 0.2.0'
end
